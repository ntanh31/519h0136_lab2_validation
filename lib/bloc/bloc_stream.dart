import 'dart:async';

class Bloc{
  final _emailStreamController = StreamController();

  // StreamController getEmailStreamController(){
  //   return _emailStreamController;
  // }

  get changeEmail => _emailStreamController.sink.add;

  Bloc(){
    _emailStreamController.stream.listen((event) {
      print("Listening... $event");
    });
  }
}