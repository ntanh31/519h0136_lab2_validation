class Commune {
  final String idDistrict;
  final String name;
  final String idCommune;

  const Commune(
      {required this.idDistrict, required this.name, required this.idCommune});

  factory Commune.fromJson(Map<String, dynamic> json) {
    return Commune(
      idDistrict: json['idDistrict'],
      name: json['name'],
      idCommune: json['idCommune'],
    );
  }
}
