class Province {
  final String idProvince;
  final String name;

  const Province({required this.idProvince, required this.name});

  factory Province.fromJson(json) {
    return Province(
      idProvince: json['idProvince'],
      name: json['name'],
    );
  }
}
