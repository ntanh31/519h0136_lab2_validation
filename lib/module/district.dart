class District {
  final String idDistrict;
  final String name;
  final String idProvince;

  const District(
      {required this.idDistrict, required this.name, required this.idProvince});

  factory District.fromJson(Map<String, dynamic> json) {
    return District(
      idDistrict: json['idDistrict'],
      name: json['name'],
      idProvince: json['idProvince'],
    );
  }
}
