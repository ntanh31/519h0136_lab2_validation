import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:week4/validation/mixin_common_validation.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../bloc/bloc_stream.dart';
import 'package:week4/module/province.dart';
import 'package:week4/module/district.dart';
import 'package:week4/module/commune.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Login Me",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Register Form"),
        ),
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SingleChildScrollView(
            child: LoginScreen(),
          )
        ),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;
  late String name;
  late String address;
  late String phone;
  late String emailError = "";
  DateTime date = DateTime.now();
  var bloc = Bloc();

  late List listProvince = [];
  late List listDistrict = [];
  late List listCommune = [];

  Future fetProvince() async {
    final response = await http.get(Uri.parse('https://vietnam-location-api.up.railway.app/province'));

    if (response.statusCode == 200) {
      List<dynamic> list = jsonDecode(response.body);
      List<Province> listParse = list.map((json) => Province.fromJson(json)).toList();

      setState(() {
        listProvince = listParse;
      });

    }
    else {
      throw Exception('Thất Bại');
    }
  }

  Future fetchDistrict(String idProvince) async {
    final response = await http.get(Uri.parse('https://vietnam-location-api.up.railway.app/district?idProvince=$idProvince'));

    if (response.statusCode == 200) {
      List<dynamic> list = jsonDecode(response.body);
      List<District> listParse = list.map((tagJson) => District.fromJson(tagJson)).toList();

      setState(() {
        listDistrict = listParse;
      });
    } else {
      throw Exception('Thất Bại');
    }
  }

  Future fetchCommune(String idDistrict) async {
    final response = await http.get(Uri.parse('https://vietnam-location-api.up.railway.app/commune?idDistrict=$idDistrict'));

    if (response.statusCode == 200) {
      List<dynamic> list = jsonDecode(response.body);
      List<Commune> listParse = list.map((tagJson) => Commune.fromJson(tagJson)).toList();

      setState(() {
        listCommune = listParse;
      });
    } else {
      throw Exception('Thất Bại');
    }
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20)),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 20)),
            nameField(),
            Container(margin: EdgeInsets.only(top: 20)),
            phoneField(),
            Container(margin: EdgeInsets.only(top: 20)),
            Row(
              children: [
                Text("Ngày Sinh: ", style: TextStyle(fontSize: 20),),
                ElevatedButton(
                  onPressed: () async{
                      DateTime? newDate = await showDatePicker(
                          context: context,
                          initialDate: date,
                          firstDate: DateTime(1900, 1, 1),
                          lastDate: DateTime.now()
                      );

                      if(newDate == null) return;

                      setState(() {
                        date = newDate;
                      });
                    },
                  child: Wrap(
                    children: <Widget>[
                      Icon(
                        Icons.calendar_month,
                        color: Colors.black87,
                        size: 24.0,
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.transparent, // background
                  ),
                ),
                SizedBox(
                  width:10,
                ),
                Text("${date.day}/${date.month}/${date.year}", style: TextStyle(fontSize: 20),),
              ],
            ),
            Container(margin: EdgeInsets.only(top: 20)),
            fieldProvince(),
            Container(margin: EdgeInsets.only(top: 20)),
            fieldDistrict(),
            Container(margin: EdgeInsets.only(top: 20)),
            fieldCommune(),
            Container(margin: EdgeInsets.only(top: 20)),
            addressField(),
            Container(margin: EdgeInsets.only(top: 20)),
            loginButton(),
          ],
        ),
      ),
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        hintText: "Email",
        errorText: emailError
      ),
      //validator: validateEmail,

      onSaved: (value){
        email = value as String;
      },

      onChanged: (value){
        print('Onchange email: $value');
        bloc.changeEmail(value);

        if(value.isEmpty){
          emailError = "Please enter email";
        }

        if(!value!.contains("@") || !value!.contains(".")){
          emailError = "Please input valid email";
        }

        emailError = "";

        setState(() {
          print("Update error email");
        });
      },
    );
  }

  Widget nameField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          hintText: "Tên"
      ),

      validator: validateName,

      onSaved: (value){
        name = value as String;
      },
    );
  }

  Widget addressField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.home),
          hintText: "Địa Chỉ"
      ),
      validator: validateAddress,
      onSaved: (value){
        address = value as String;
      },
    );
  }

  Widget phoneField(){
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          icon: Icon(Icons.phone_android),
          hintText: "SĐT"
      ),

      validator: validatePhone,

      onSaved: (value){
        phone = value as String;
      },
    );
  }

  Widget passwordField(){
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          hintText: "Password"
      ),

      validator: validatePassword,

      onSaved: (value){
        password = value as String;
      },
    );
  }

  Widget fieldProvince() {
    return DropdownButton<dynamic>(
      hint: const Text("Chọn Thành phố"),
      items: listProvince.map((province) {
        return DropdownMenuItem<dynamic>(
          value: province.idProvince,
          child: Text(province.name),
        );
      }).toList(),
      onChanged: (value) {
        fetchDistrict(value);
      },
    );
  }

  Widget fieldDistrict() {
    return DropdownButton<dynamic>(
        hint: const Text("Chọn Quận Huyện"),
      items: listDistrict.map((district) {
        return DropdownMenuItem<dynamic>(
          value: district.idDistrict,
          child: Text(district.name),
        );
      }).toList(),
      onChanged: (value) {
        fetchCommune(value);
      },
    );
  }

  Widget fieldCommune() {
    return DropdownButton<dynamic>(
      hint: const Text("Chọn Phường Xã"),
      items: listCommune.map((commune) {
        return DropdownMenuItem<dynamic>(
          value: commune.idCommune,
          child: Text(commune.name),
        );
      }).toList(),
      onChanged: (value) {},
    );
  }

  Widget loginButton(){
    return ElevatedButton(
      child: Text("Register", style: TextStyle(fontSize: 20),),
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();

          print("Email: $email");
          print("Password: $password");
        }
      },
    );
  }
}
